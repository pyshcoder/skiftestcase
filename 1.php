<?php
namespace App;

require __DIR__ . '/vendor/autoload.php';

$nats = NatsAdapter::getInstance();

$nats->subscribe(
    'test',
    function ($message) {
        printf("Recieved: %s\r\n", $message->getBody());
    }
);

$nats->publish('test', 'Hello, world');

$nats->wait(1);