<?php
$baseUrl = 'https://jsonplaceholder.typicode.com/comments/{id}'; // Заглушка с фейковыми данными
$commentIDs = range(1, 100); // Хочу загрузить 100 комментариев

$stillRunning = null;
$curlHandlers = [];
$multiHandler = curl_multi_init(); 

for($i = 0; $i < count($commentIDs); $i++) {
    $handler = curl_init();
    curl_setopt($handler, CURLOPT_URL, str_replace('{id}', $commentIDs[$i], $baseUrl)); // Замена плейсхолдера
    curl_setopt($handler, CURLOPT_HEADER, false);
    curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handler, CURLOPT_SSL_VERIFYPEER, false);
    
    curl_multi_add_handle($multiHandler, $handler);
    
    $curlHandlers[] = $handler;
}

do {
    $code = curl_multi_exec($multiHandler, $stillRunning); // Запускаем все потоки
} while ($code == CURLM_CALL_MULTI_PERFORM);

while ($stillRunning && $code == CURLM_OK) {
    if (curl_multi_select($multiHandler) == -1) {
        usleep(100); // Отдаем управлению чтобы не грузить проц
    }

    do {
        $code = curl_multi_exec($multiHandler, $stillRunning);
    } while ($code == CURLM_CALL_MULTI_PERFORM);
}

$loadedComments = []; // Массив для загруженных данных
foreach($curlHandlers as $handler) {
    $loadedComments[] = curl_multi_getcontent($handler);
    curl_multi_remove_handle($multiHandler, $handler); // Отцепляем потоки
}

// Теперь можем сделать что-нибудь с загруженными данными, например вывести их на экран

foreach($loadedComments as $comment) {
    print $comment . '\r\n';
}

curl_multi_close($multiHandler);  // Закрываем главный поток