<?php
namespace common\behaviors;

use yii\web\Controller;

class MetaTag extends \yii\base\Behavior {
    public $metaTags;

    public function events() {
        return [
            Controller::EVENT_BEFORE_ACTION => 'addMetaTags',
        ];
    }
    
    public function addMetaTags($event) {
        if(is_null($this->metaTags) || !is_array($this->metaTags)) {
            return $event->isValid;
        }

        foreach($this->metaTags as $name => $content) {
            \Yii::$app->view->registerMetaTag([
                'name' => $name,
                'content' => $content,
            ], $name);
        }
    }
}

/*
    Using:
        
    'metatag' => [
        'class' => \common\behaviors\MetaTag::className(),
        'metaTags' => [
            'description' => 'test',
            'keywords' => 'test',
        ]
    ],
*/