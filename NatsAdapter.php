<?php
namespace App;

final class NatsAdapter {
    private static $_instance = null;

    // для упрощения настройки подключения тут, по правильному они должны храниться где-то в конфиге и подтягиваться отдельно
    private static $_host = '127.0.0.1';
    private static $_port = '4222';

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    static public function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new \Nats\Connection(new \Nats\ConnectionOptions([
                'host' => self::$_host,
                'port' => self::$_port,
            ]));

            self::$_instance->connect();
        }

        return self::$_instance;
    }
}