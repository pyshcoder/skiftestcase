<?php

trait BatchInsert {
    public function batchInsertUpdate(array $updateColumns, array $dataToInsert) {
        if(is_null($dataToInsert) || is_null($updateColumns)) {
            return false; // todo: throw exception
        }
        
        $onDuplicateKeyValues = [];
        
        foreach($updateColumns as $column) {
            $column = $this->db->quoteColumnName($column);
            $onDuplicateKeyValues[] = $column . ' = VALUES(' . $column . ')';
        }
        
        $sqlQuery = $this->db->getQueryBuilder()->batchInsert($this->tableName(), $updateColumns, $dataToInsert);
        $sqlQuery .= ' ON DUPLICATE KEY UPDATE ' . implode(', ', $onDuplicateKeyValues);
        
        return $this->db->createCommand($sqlQuery)->execute();
    }
}