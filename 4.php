<?php

// Ну из простейшего, этого число фибоначчи

function fibonacci() {
    $lastNumber = 0;
    $currentNumber = 1;
    
    yield $currentNumber;
    
    while(true) {
        $currentNumber = $lastNumber + $currentNumber;
        $lastNumber = $currentNumber - $lastNumber;
        
        yield $currentNumber;
    }
}

$max = 30; // сколько чисел хотим получить

foreach(fibonacci() as $number) {
    if($max == 0) {
        return;
    }
    $max--;
    print $number . ', ';
}

// Чуть посложнее, загрузка файла по линиям ()

function getLineGenerator($file) {
    $file = fopen($file, 'r');
    
    if (!$file) throw new Exception();
    
    try {
        while ($line = fgets($file)) {          
            yield $line;
        }
    } finally {
        fclose($file); // обязательно закрыть файл в блоке finally, вдруг цикл прервут раньше конца файла   
    }
}

$max = 30; // хочу прочитать только первые 30 строк

// Работаю из под macos, поэтому у меня есть файлик с логом по этому пути
foreach(getLineGenerator('/var/log/system.log') as $line) { 
    if($max == 0) {
        return;
    }
    $max--;
    
    print $line . '<br>';
}